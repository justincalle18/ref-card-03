# Architecture Ref. Card 03

## Einleitung

Es wurde den Auftrag gestellt bis auf den 06.06.2023. Der Auftrag ist, unser Gitlab abzugeben mit einem README.md in welchen die Schritte zur Installation benötigt sind.

## Erstellen der Files

### Dockerfile
```sh
FROM maven:3-openjdk-11-slim
COPY src /src
COPY pom.xml /
RUN mvn -f pom.xml clean package
RUN mv /target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

### .gitlab-ci.yml
```sh
image: docker:23.0.4

variables:
  
  DOCKER_HOST: tcp://docker:2375
  #
  # This instructs Docker not to start over TLS.
  DOCKER_TLS_CERTDIR: ""
  #
  # AWS_ACCESS_KEY_ID
  # AWS_SECRET_ACCESS_KEY
  # AWS_SESSION_TOKEN
  # AWS_DEFAULT_REGION
  # CI_AWS_ECR_REGISTRY
  # CI_AWS_ECR_REPOSITORY_NAME

services:
  - docker:23.0.4-dind

before_script:
  - docker info

publish:
  stage: build
  before_script:
    - apk add --no-cache py3-pip
    - pip install awscli
    - aws --version
    - aws ecr get-login-password | docker login --username AWS --password-stdin $CI_AWS_ECR_REGISTRY

  script:
    - docker build --cache-from $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest -t $CI_AWS_ECR_REPOSITORY_NAME .
    - docker tag $CI_AWS_ECR_REPOSITORY_NAME:latest $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest
    - docker push $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest

  # Run this job in a branch where a Dockerfile exists
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - Dockerfile
```


## Installation

1. Forke dieses Projekt von Gitlab, welches vom Auftraggeber zur Verfügung gestellt wurde. Man muss dieses Projekt Forken, um die veränderungen auf seinem eigenen Repository zu speichern.
2. Pushen Sie die erstellten Files hoch.
```sh
git add .
```
```sh
git commit -m "<Kommentar>"
```
```sh
git push
```
3. Erstellen Sie nun unter AWS ein neues Repository in ECR. 
4. Gehen Sie auf Gitlab auf die Einstellungen ihres Repositorys und danach auf CI/CD. Erstellen Sie hier nun einen Runner. Es soll beachtet werden, dass die Checkbox "Run untagged jobs" aktiviert ist, um Jobs zu starten, die nicht mit bestimmten Tags versehen sind. Schlussendlich muss für den nächsten Schritt den Token kopiert werden.
5. Als nächstes wird der Container auf Docker erstellt.

```sh
docker run -d --name gl-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner
```
Startet einen Container mit dem Namen: "gl-runner" und verwendet das gefügte Image.

```sh
docker exec gl-runner gitlab-runner register --url https://gitlab.com --token < TOKEN > --executor docker --docker-image docker:23.0.4 --docker-privileged --non-interactive 
```
Verbindet den Container mit dem Gitlab Runner.

```sh
docker restart gl-runner 
```
6. Erstellen Sie nun die Variabeln auf Gitlab unter Einstellungen / CI/CD. Benutzen Sie Folgende Variabeln und deaktivieren Sie "Protect variable":
* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY
* AWS_SESSION_TOKEN
* AWS_DEFAULT_REGION
* CI_AWS_ECR_REGISTRY
* CI_AWS_ECR_REPOSITORY_NAME
Diese Variabeldefinitionen finden Sie in AWS bei AWS Details und danach auf AWS CLI. Für die letzten drei Variabeln wird nun auf das bereits erstellte Repository zugegriffen. Alles kann von der URL abgelesen werden. Die Variabeln können beim abschalten der AWS Systemen verändern. Somit muss bei jedem hochfahren neue Variabeln erstellt werden.
7. Schliesslich können die Pipelines gestartet werden und die Installation sollte Erfolgreich sein. Das starten der Pipelines kann einige Zeit in Anspruch nehmen.

8. Mithilfe von ECS ein Cluster erstellen, Aufgabendefinitionen erstellen mit dem Image, was wir in ECR erstellt haben. Als letztes muss noch mit dem RDS service eine Datenbank erstellt werden. Zusätzlich muss das Dockerfile und .gitlab-ci.yml file erweitert werden:
* Dockerfile
```sh
FROM maven:3-openjdk-11-slim

COPY src /src
COPY pom.xml /

RUN mvn -f pom.xml clean package
RUN mv /target/*.jar app.jar

ENV DB_URL=$DB_URL
ENV DB_USERNAME=$DB_USERNAME
ENV DB_PASSWORD=$DB_PASSWORD

ENTRYPOINT ["java","-jar","/app.jar", "--DB_URI=${DB_URL}", "--DB_USER=${DB_USERNAME}", "--DB_PASS=${DB_PASSWORD}"]
```
* .gitlab-ci.yml
```sh
deploy:
  stage: deploy
  image: python:3.7
  before_script:
    - pip install awscli
    - aws --version
  script:
    - aws ecs update-service --cluster $CI_AWS_ECS_CLUSTER_NAME --service $CI_AWS_ECS_SERVICE_NAME --force-new-deployment
```
